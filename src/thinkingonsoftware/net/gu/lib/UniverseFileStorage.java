/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.lib;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author LoSboccacc
 */
public class UniverseFileStorage implements UniverseStorage {

    public UniverseFileStorage() {
    }
    
    
    final public static File saveFolder = new File (new File(
            System.getProperty("user.dir"), "GrandUnification"), "saves");

    @Override
    public Collection<UniverseHandler> listUniverses() {
        if (!saveFolder.exists()){
            saveFolder.mkdirs();
        }
        
        File [] aSaveFiles = saveFolder.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                if (pathname.isFile() && pathname.exists()){
                   if (pathname.getName().endsWith(".gus")) return true;
                }
                return false;
            }
        });
        
        ArrayList<UniverseHandler> a = new ArrayList<UniverseHandler>();
        if (aSaveFiles != null)
        for (File fileSave : aSaveFiles ) {
            
            a.add(new UniverseFileHandler(saveFolder, fileSave.getName().replace(".gus", "")) {});
        }
        
        
        
        return a;
    }

    @Override
    public UniverseHandler createNewHandler(String name) {
            return new UniverseFileHandler(saveFolder, name);
    }
        
}
