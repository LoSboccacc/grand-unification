/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.lib;

import thinkingonsoftware.net.gu.model.GameParameters;
import thinkingonsoftware.net.gu.model.GameStatus;

/**
 * 
 * Handle game files, maps a game to each saved state.
 * 
 * @author LoSboccacc
 */
public interface UniverseHandler  {

    GameStatus newGame(GameParameters p);

    void deleteGame();

    GameStatus loadGame() throws Exception;

    void saveGame(GameStatus current) throws Exception;

}
