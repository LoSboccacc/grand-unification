/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.lib;

import java.util.Collection;

/**
 *
 * @author LoSboccacc
 */
public interface UniverseStorage {

    Collection<UniverseHandler> listUniverses();
    UniverseHandler createNewHandler(String name);
}
