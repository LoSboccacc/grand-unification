/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.lib;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import thinkingonsoftware.net.gu.model.GameParameters;
import thinkingonsoftware.net.gu.model.GameStatus;

/**
 *
 * @author LoSboccacc
 */
public class UniverseFileHandler implements UniverseHandler {

    String name;
    private final File file;
    private Properties prp;
    private final Logger logger = Logger.getLogger(UniverseHandler.class.getCanonicalName());

    public UniverseFileHandler(File fileSave, String gameName) {
        fileSave.mkdir();
        this.file = new File(fileSave, gameName.concat(".gus"));
        this.name = gameName;
        logger.info("Handler for " + gameName + " in " + file.getAbsolutePath());


    }

    public GameStatus loadGame() throws Exception {

        GameStatus gs = null;

        ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));

        Object o = is.readObject();
        is.close();
        if (o instanceof GameStatus) {
            return (GameStatus) o;
        } else {
            throw new InvalidObjectException("Bad Game Status found: " + o.getClass().getName());
        }

    }

    public void saveGame(GameStatus status) throws Exception {
        file.getParentFile().mkdirs();
       
        ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
        os.writeObject(status);
        os.close();
    }

    @Override
    public String toString() {
        return name == null ? "" : name;
    }

    public void deleteGame() {
        try {
            if (file != null) {
                if (file.exists()) {
                    delete(file);
                } else {
                    logger.log(Level.WARNING, "File not found");
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(UniverseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new IOException("Failed to delete file: " + f);
        }
    }

    public GameStatus newGame(GameParameters p) {
        deleteGame();
        Logger.getLogger(UniverseHandler.class.getName()).log(Level.INFO, "Creating new Game with parameters" + p);
        GameStatus gs = new GameStatus(p);
        return gs;
    }
}
