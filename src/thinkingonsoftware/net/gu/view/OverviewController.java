/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import thinkingonsoftware.net.gu.model.GameStatus;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.panels.OverviewPanel;

/**
 *
 * @author LoSboccacc
 */
public class OverviewController implements  ViewController{
    

    private final GameStatus status;

    public OverviewController(GameStatus s) {
     this.status = s;

    }
    
    
    
    @Override
    public JPanel create() {
        return new OverviewPanel(this);
    }

    @Override
    public JPanel restore(Object memento) {
        return new OverviewPanel(this);
    }

    @Override
    public Object destroy() {
        return null;
    }

    public void openNavigationPanel() {
        RootNavigationController.push( new NavigationController(status.getPlayer(),status.getCurrentUniverse()));
    }

    public void openFleetPanel() {
         RootNavigationController.push( new FleetOverviewController(status.getPlayer(),status.getCurrentUniverse()));
    }
    public void openFactoryPanel() {
         RootNavigationController.push( new FactoryOverviewController(status.getPlayer(),status.getCurrentUniverse()));
    }

    public void openDesignPanel() {
         RootNavigationController.push( new DesignPanelController(status.getPlayer(),status.getCurrentUniverse()));
    }
    
}
