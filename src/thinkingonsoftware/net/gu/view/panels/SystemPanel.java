/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NavigationPanel.java
 *
 * Created on 18-Oct-2011, 22:59:23
 */
package thinkingonsoftware.net.gu.view.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import thinkingonsoftware.net.gu.model.universe.dao.Body;
import thinkingonsoftware.net.gu.model.universe.dao.BodyType;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.RootNavigationController;
import thinkingonsoftware.net.gu.view.SystemController;

/**
 *
 * @author LoSboccacc
 */
public class SystemPanel extends javax.swing.JPanel {

    private SystemController owner;
    double scale = 1.0d;
    private StarSystem system;
    private final Universe current;

    /** Creates new form NavigationPanel */
    public SystemPanel(StarSystem s, SystemController sc, Universe u) {
        initComponents();
        this.current = u;

        addComponents(sc, s);

    }

    public final void addComponents(SystemController sc, StarSystem s) {
        this.owner = sc;
        this.system = s;
        JPanel jPanelUniverseView = new JPanel();
        jPanelUniverseView.setLayout(null);
        int minx = Integer.MAX_VALUE;
        int miny = Integer.MAX_VALUE;
        int maxx = Integer.MIN_VALUE;
        int maxy = Integer.MIN_VALUE;


        jPanelUniverseView.removeAll();
        
        
        
        
        
        for (final Body b : s.getBodyes()) {
            Point p = b.getDrawPosition();
            p.x = (int) (p.x * scale);
            p.y = (int) (p.y * scale);
            //if (p.x<0) throw new IllegalArgumentException("Invalid system x coordinate");
            //if (p.y<0) throw new IllegalArgumentException("Invalid system y coordinate");
            if (p.x < minx) {
                minx = p.x;
            }
            if (p.x > maxx) {
                maxx = p.x;
            }
            if (p.y < miny) {
                miny = p.y;
            }
            if (p.y > maxy) {
                maxy = p.y;
            }
        }

        maxx = Math.max(Math.abs(minx), Math.abs(maxx));
        maxy = Math.max(Math.abs(miny), Math.abs(maxy));
        minx = -maxx;
        miny = -maxy;

        for (final Body bd : s.getBodyes()) {

            Point p = bd.getDrawPosition();
            p.x = (int) (p.x * scale);
            p.y = (int) (p.y * scale);
            p.x = p.x - minx + 64;
            p.y = p.y - miny + 64;

            final JButton b = new JButton() {
                Image i = bd.getImage();
                @Override
                public void paint(Graphics g) {
                    g.drawImage(i, 0, 0, this.getWidth(), this.getHeight(),null);
                    super.paint(g);
                    
                }
                
                
            };
            //b.setToolTipText(s.trueName);
            JLabel l = new JLabel(bd.getTrueName());
            //l.setLocation(p.x+16, p.y+32);
            l.setBounds(p.x - 16, p.y - 32, 256, 32);
            l.setForeground(Color.WHITE);
            jPanelUniverseView.add(l);
            b.setText("");
            
            b.setRolloverEnabled(true);
            b.setRolloverIcon(new ImageIcon("data/templatebody.png"));
            b.setPressedIcon(new ImageIcon("data/templatebody.png"));
            if (bd.getType()== BodyType.STAR) {                
                b.setBounds(p.x - 128, p.y - 64, 256, 256);
            } else {
                b.setBounds(p.x - 16, p.y - 16, 128, 128);
            }
            b.setOpaque(false);
            b.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });
            jPanelUniverseView.add(b);
        }


        jPanelUniverseView.setPreferredSize(new Dimension(128 + maxx - minx+256, 128 + maxy - miny+256));

        jPanelUniverseView.revalidate();
        JPanel t = new JPanel();
        t.setLayout(new GridBagLayout());
        t.add(jPanelUniverseView);
        jScrollPane1.setViewportView(t);

        jScrollPane1.revalidate();
        jScrollPane1.repaint();
        int sx = RootNavigationController.getScreenDimension().getSize().width / 2;
        int sy = RootNavigationController.getScreenDimension().height / 2;

        jPanelUniverseView.scrollRectToVisible(new Rectangle((maxx - minx) / 2 - sx + 64, (maxy - miny) / 2 - sy + 64, 2 * sx, 2 * sy));


        new DragScrollHandler(jScrollPane1.getViewport());
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.LINE_AXIS));

        jScrollPane1.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                jScrollPane1MouseWheelMoved(evt);
            }
        });
        add(jScrollPane1);
    }// </editor-fold>//GEN-END:initComponents

    private void jScrollPane1MouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_jScrollPane1MouseWheelMoved
        System.out.println(evt);
        if (evt.getWheelRotation() >0) {
            scale = scale/1.1d;
        }else {
            scale = scale*1.1d;
        }
        addComponents(owner, system);
        evt.consume();
    }//GEN-LAST:event_jScrollPane1MouseWheelMoved
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
    class DragScrollHandler extends MouseAdapter {

        private JComponent component;
        private Point pressed, here;
        private Rectangle visiRect;

        public DragScrollHandler(JComponent component) {
            
            for (MouseListener ml : component.getMouseListeners()) {
                component.removeMouseListener(ml);
            }
            for (MouseMotionListener ml : component.getMouseMotionListeners()) {
                component.removeMouseMotionListener(ml);
            }
            
            this.component = component;
            component.addMouseListener(this);
            component.addMouseMotionListener(this);
        }

        public void dispose() {
            component.removeMouseListener(this);
            component.removeMouseMotionListener(this);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            pressed = e.getPoint();
            visiRect = component.getVisibleRect();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            here = e.getPoint();
            visiRect.x += (pressed.x - here.x);
            visiRect.y += (pressed.y - here.y);
            component.scrollRectToVisible(visiRect);
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    Rectangle newRect = component.getVisibleRect();
                    pressed.x += newRect.x - visiRect.x;
                    pressed.y += newRect.y - visiRect.y;
                    visiRect = newRect;
                }
            });
        }
    }
}
