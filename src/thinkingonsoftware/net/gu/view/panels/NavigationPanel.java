/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NavigationPanel.java
 *
 * Created on 18-Oct-2011, 22:59:23
 */
package thinkingonsoftware.net.gu.view.panels;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolTip;
import javax.swing.JViewport;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
import thinkingonsoftware.net.gu.model.GameStatus;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.NavigationController;
import thinkingonsoftware.net.gu.view.RootNavigationController;
import thinkingonsoftware.net.gu.view.widgets.BodyWidget;

/**
 *
 * @author LoSboccacc
 */
public class NavigationPanel extends javax.swing.JPanel {
    private final NavigationController owner;

    /** Creates new form NavigationPanel */
    public NavigationPanel(Player player, NavigationController nc, Universe current) {
        this.owner = nc;
        initComponents();
        JPanel jPanelUniverseView = new JPanel();
        jPanelUniverseView.setLayout(new GridBagLayout());
        
        
        jPanelUniverseView.removeAll();
        for (final StarSystem s : current.getSystems()) {
            
            
            final BodyWidget b = BodyWidget.createForStarSystem(s);
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = s.x;
            gbc.gridy = s.y;
            b.setSize(128, 128);
            b.setMaximumSize(new Dimension(128,128));
            b.setMinimumSize(new Dimension(128,128));
            b.setPreferredSize(new Dimension(128,128));
            b.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    owner.openSystem(s);
                }
            });

            jPanelUniverseView.add(b,gbc);
        }
        
        
        jPanelUniverseView.revalidate();
        JPanel t =new JPanel();
        t.setLayout(new GridBagLayout());
        t.add(jPanelUniverseView);
        jScrollPane1.setViewportView(t);

        jScrollPane1.revalidate();
        jScrollPane1.repaint();
        
        int sx = RootNavigationController.getScreenDimension().getSize().width/2; 
        int sy = RootNavigationController.getScreenDimension().height/2; 
        
        
        
        
        new DragScrollHandler(jScrollPane1.getViewport());
        
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.LINE_AXIS));
        add(jScrollPane1);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
class DragScrollHandler extends MouseAdapter {
 
  private JComponent component;
  private Point pressed, here;
  private Rectangle visiRect;
 
  public DragScrollHandler(JComponent component) {
    this.component = component;
    component.addMouseListener(this);
    component.addMouseMotionListener(this);
  }
 
  public void dispose() {
    component.removeMouseListener(this);
    component.removeMouseMotionListener(this);
  }
 
  @Override
  public void mousePressed(MouseEvent e) {
    pressed = e.getPoint();
    visiRect = component.getVisibleRect();
  }
 
  @Override
  public void mouseDragged(MouseEvent e) {
    here = e.getPoint();
    visiRect.x += (pressed.x - here.x);
    visiRect.y += (pressed.y - here.y);
    component.scrollRectToVisible(visiRect);
    SwingUtilities.invokeLater(new Runnable() {
 
      public void run() {
        Rectangle newRect = component.getVisibleRect();
        pressed.x += newRect.x - visiRect.x;
        pressed.y += newRect.y - visiRect.y;
        visiRect = newRect;
      }
    });
  }
}
}
