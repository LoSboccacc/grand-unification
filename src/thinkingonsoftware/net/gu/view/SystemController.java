/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.panels.NavigationPanel;
import thinkingonsoftware.net.gu.view.panels.SystemPanel;

/**
 *
 * @author LoSboccacc
 */
public class SystemController implements  ViewController{
    public SystemPanel navigationPanel;
    private StarSystem  system;
    private final Universe universe;

    public SystemController(StarSystem s, Universe u) {
        this.system = s;
        this.universe = u;
    }

    @Override
    public JPanel create() {
        return navigationPanel  = new SystemPanel(system, this,universe);
    }

    @Override
    public JPanel restore(Object memento) {
        system = (StarSystem) memento;
        return navigationPanel  = new SystemPanel(system, this, universe);
    }

    @Override
    public Object destroy() {
        Object memento = system;
        system = null;
        navigationPanel = null;
        return memento;
    }   
}
