/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

/**
 *
 * @author LoSboccacc
 */
class TutorialController {

    private static TutorialStatus status = TutorialStatus.DISABLED;

    TutorialController() {
    }

    void shown(ViewController c) {
        if (c instanceof GameController) {
            return;
        }
        switch (status) {
            case TO_BE_INITIALIZED:
                init(c);
                break;
            case CREATED:
                created(c);
                break;
            case OPEN_FACTORY:
                openFactoryPanel(c);
                break;
            case EARTH_SELECTED:
                earthSelected(c);
                break;

            case OPEN_DESIGN_TAB:
                openDesignTab(c);
                break;
            case DESIGN_ENGINE:
                designEngine(c);
                break;
                
            case DISABLED:
                break;
            default:
                RootNavigationController.showTutorialAlert("Tutorial is broken o_O");

        }
    }

    private void init(ViewController c) {
        if (c instanceof GameController) {
            RootNavigationController.showTutorialAlert("Create a new game pressing new");
        }
        if (c instanceof GameCreationController) {
            RootNavigationController.showTutorialAlert("For your first game it is better to select the easy difficulty.");
        }

    }

    void setStatus(TutorialStatus tutorialStatus) {
        System.out.println("Tutorial Status: " + tutorialStatus);
        this.status = tutorialStatus;
        RootNavigationController.tutorialStatusChanged(status);


      
    }
    
    void changeStatus(TutorialStatus tutorialStatus) {
        showChangeSpecificMessage(tutorialStatus);
        performStatusChange(tutorialStatus);
       
    }


    private void created(ViewController c) {
        if (c instanceof OverviewController) {
            setStatus(TutorialStatus.OPEN_FACTORY);
            RootNavigationController.showTutorialAlert("Welcome blurb");
            RootNavigationController.showTutorialAlert("From this panel, you have access and control on all the information you'll need");
            RootNavigationController.showTutorialAlert("Select the Factories panel to show an overview of your colonies");
        }

    }

    private void openFactoryPanel(ViewController c) {

        if (c instanceof OverviewController) {
            RootNavigationController.showTutorialAlert("Select the Factories panel to show an overview of your colonies.");
            return;
        }
        if (c instanceof FactoryOverviewController) {
            RootNavigationController.showTutorialAlert("Find and select planeth earth");
            RootNavigationController.showTutorialAlert("It starts with just some basic production and mining facilities.");
            return;
        }
        RootNavigationController.showTutorialAlert("Press back to return to the Overview panel");
    }

    private void earthSelected(ViewController c) {
        if (c instanceof OverviewController) {
            RootNavigationController.showTutorialAlert("Let's build an exploration ship, Mars looks a promising planet for rare metals.");
            RootNavigationController.showTutorialAlert("Open the design panel to detail the ship specifications");

            return;
        }
        if (c instanceof DesignPanelController) {
            setStatus(TutorialStatus.OPEN_DESIGN_TAB);           
            RootNavigationController.showTutorialAlert("This panel show an overview of you ship designs");
            RootNavigationController.showTutorialAlert("But to build one, you'll first need to specify its components");
            RootNavigationController.showTutorialAlert("Open the Components tab");
            
            return;            
        }
        RootNavigationController.showTutorialAlert("Press back to return to the Overview panel");
    }

    private void openDesignTab(ViewController c) {
         if (c instanceof OverviewController) {
             RootNavigationController.showTutorialAlert("Open the design panel");
            return;
        }
        if (c instanceof DesignPanelController) {
             RootNavigationController.showTutorialAlert("Select the Components tab");
            return;
        }
        
        RootNavigationController.showTutorialAlert("Press back to return to the Overview panel");
    }

    private void designEngine(ViewController c) {
        if (c instanceof OverviewController) {
             RootNavigationController.showTutorialAlert("Open the design panel");
            return;
        }
        if (c instanceof DesignPanelController) {
             RootNavigationController.showTutorialAlert("Select the Components tab and press New");
            return;
        }
        
        RootNavigationController.showTutorialAlert("Press back to return to the Overview panel");
       
    }

    public void showChangeSpecificMessage(TutorialStatus tutorialStatus) {
        //Status change message
        switch (tutorialStatus) {
            case EARTH_SELECTED:
                RootNavigationController.showTutorialAlert("Look at the planet structures: just some power plant and mines from the pre space era");
                RootNavigationController.showTutorialAlert("We will build factories soon enough; after you've read the planet stat return back to the overview panel.");
            break;
            case DESIGN_ENGINE:
                RootNavigationController.showTutorialAlert("Press New to open the component design panel");                
            break;

        }
    }

    public void performStatusChange(TutorialStatus tutorialStatus) {
        switch (status) {
            case OPEN_DESIGN_TAB:
                switch (tutorialStatus) {
                    case COMPONENT_PANEL_SELECTED:
                        changeStatus(TutorialStatus.DESIGN_ENGINE);
                        break;
                }

                break;
            default:
                setStatus(tutorialStatus);

        }
    }
}
