/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import de.muntjak.tinylookandfeel.Theme;
import de.muntjak.tinylookandfeel.ThemeDescription;
import de.muntjak.tinylookandfeel.TinyLookAndFeel;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import thinkingonsoftware.net.gu.lib.UniverseFileHandler;
import thinkingonsoftware.net.gu.lib.UniverseFileStorage;
import thinkingonsoftware.net.gu.view.panels.RootFrame;

/**
 *
 * handles navigation from a stack of controllers
 *
 * @author LoSboccacc
 */
public class RootNavigationController {
    //pure static class

    static final Stack<Object> mementos = new Stack<Object>();
    static final Stack<ViewController> controllers = new Stack<ViewController>();
    static ViewController current;
    static TutorialController tut = new TutorialController();
    private static RootFrame root;
    private static GameController gc;

    public static void quit() {
        gc.quitGame();
    }

    public static Dimension getScreenDimension() {
        return root.getCenterPanel().getSize();
    }

    static void replace(ViewController c) {
        current = controllers.pop();
        push(c);
    }

    static void showErrorAlert(String msg) {
        JOptionPane.showMessageDialog(root, msg, "Error", JOptionPane.ERROR_MESSAGE);

    }

    static boolean showYNDialog(String msg) {
        int ret = JOptionPane.showConfirmDialog(root, msg, "Error", JOptionPane.YES_NO_OPTION);
        return ret == JOptionPane.YES_OPTION;
    }

    static void showMessageAlert(String msg) {
        JOptionPane.showMessageDialog(root, msg, "", JOptionPane.INFORMATION_MESSAGE);

    }
    static Queue<String> msgqueue = new ArrayDeque<String>();

    static void showTutorialAlert(final String msg) {
        JOptionPane.showMessageDialog(root, msg, "Tutorial", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * change status without showing status change message (i.e. at load)
     */
    static void setTutorialStatus(TutorialStatus tutorialStatus) {
        tut.setStatus(tutorialStatus);
    }
    /**
     * change the status and show status change messages
    */
    static void changeTutorialStatus(TutorialStatus tutorialStatus) {
        tut.changeStatus(tutorialStatus);
    }

    static void tutorialStatusChanged(TutorialStatus status) {
        if (gc != null) {
            gc.tutorialStatusChanged(status);
        }
    }

    private RootNavigationController() {
    }

    public static void main(String[] args) {
        try {
            /*
             for (ThemeDescription d : Theme.getAvailableThemes()) {
             if ("Nightly".equals(d.getName())) {
             Theme.loadTheme(d);
             }
             }*/

            // continuous layout on frame resize
            Toolkit.getDefaultToolkit().setDynamicLayout(true);
            // no flickering on resize
            System.setProperty("sun.awt.noerasebackground", "true");


            UIManager.setLookAndFeel(new TinyLookAndFeel());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JFrame.setDefaultLookAndFeelDecorated(true);	// to decorate frames
        JDialog.setDefaultLookAndFeelDecorated(true);	// to decorate dialogs

        root = new RootFrame();
        root.setUndecorated(true);

        root.setLocationByPlatform(true);
        gc = new GameController(new UniverseFileStorage());

        push(gc);
        root.setVisible(true);
        root.setBackButtonEnabled(false);
    }

    public static void push(ViewController c) {

        if (c == null) {
            throw new NullPointerException("Null Controller");
        }
        if (current != null) {
            controllers.push(current);
            mementos.push(current.destroy());
        }
        current = c;
        setView(current.create());
        tut.shown(c);
        root.setBackButtonEnabled(controllers.size() != 0);

    }

    public static void pop() {

        current = controllers.pop();
        final JPanel view = current.restore(mementos.pop());
        if (view == null) {
            throw new IllegalStateException("Null view from " + current.getClass().getSimpleName());
        }
        setView(view);
        tut.shown(current);
        root.setBackButtonEnabled(controllers.size() != 0);
    }

    private static void setView(JPanel view) {
        root.getCenterPanel().removeAll();
        root.getCenterPanel().add(view);
        root.getCenterPanel().validate();

    }
}
