/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.panels.NavigationPanel;

/**
 *
 * @author LoSboccacc
 */
public class NavigationController implements  ViewController{
    public NavigationPanel navigationPanel;
    private Player player;
    private final Universe universe;

    public NavigationController(Player player, Universe u) {
        this.player = player;
        this.universe = u;
    }

    @Override
    public JPanel create() {
        return navigationPanel  = new NavigationPanel(player, this,universe);
    }

    @Override
    public JPanel restore(Object memento) {
        player = (Player) memento;
        return navigationPanel  = new NavigationPanel(player, this,universe);
    }

    @Override
    public Object destroy() {
        Object memento = player;
        player = null;
        navigationPanel = null;
        return memento;
    }

    public void openSystem(StarSystem s) {
          RootNavigationController.push( new SystemController(s,universe));
    }
    
}
