/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

/**
 *
 * @author LoSboccacc
 */
public enum TutorialStatus {
    DISABLED,
    TO_BE_INITIALIZED,
    CREATED, OPEN_FACTORY, EARTH_SELECTED, OPEN_DESIGN_TAB,DESIGN_ENGINE, DESIGN_SENSOR, COMPONENT_PANEL_SELECTED;
}
