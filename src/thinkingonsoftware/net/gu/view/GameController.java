/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import thinkingonsoftware.net.gu.model.GameStatus;
import thinkingonsoftware.net.gu.lib.UniverseHandler;
import thinkingonsoftware.net.gu.lib.UniverseFileStorage;
import thinkingonsoftware.net.gu.lib.UniverseStorage;
import thinkingonsoftware.net.gu.model.GameParameters;
import thinkingonsoftware.net.gu.view.panels.GamePanel;

/**
 *
 * the first controller, handles save/load/creation of games
 *
 * @author LoSboccacc
 */
public class GameController implements ViewController {

    private GamePanel gamePanel;
    private final Logger logger = Logger.getLogger(GameController.class.getName());
    private UniverseStorage storage = null;
    private GameStatus s;
    private UniverseHandler h;

    GameController(UniverseStorage universeStorage) {

        storage = universeStorage;
        gamePanel = new GamePanel(this, universeStorage.listUniverses());

    }

    @Override
    public JPanel create() {
        return gamePanel;
    }

    @Override
    public JPanel restore(Object memento) {

        storage = (UniverseFileStorage) memento;
        quitGame();
        return gamePanel = new GamePanel(this, storage.listUniverses());
    }

    @Override
    public Object destroy() {

        gamePanel = null;
        return storage;
    }

    public void loadGame(UniverseHandler h) {
        this.h = h;
        if (h == null) {
           RootNavigationController.showMessageAlert("Select a saved universe");
            return;
        }
        try {
            s = h.loadGame();
            RootNavigationController.setTutorialStatus(s.getTutorialStatus());
            RootNavigationController.push(new OverviewController(s));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            boolean accept = RootNavigationController.showYNDialog("Saved Game Corrupted. Delete?");
            if (accept) {
                h.deleteGame();
                gamePanel.setGameList(storage.listUniverses());
            }
        }
    }

    public void startGame() {
        try {

           
            RootNavigationController.push(new GameCreationController(h, this));

            /*
             s = h.newGame(new GameParameters());
                
              RootNavigationController.push(new OverviewController(s));*/
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
           RootNavigationController.showErrorAlert("Error in creating a new universe");
        }
    }

    public void startGame(GameParameters p) {
        try {
             h = storage.createNewHandler(p.getGameName());
            s = h.newGame(p);
            if (p.isTutorialEnabled()){
              RootNavigationController.setTutorialStatus(TutorialStatus.CREATED);
            } else {
                              RootNavigationController.setTutorialStatus(TutorialStatus.DISABLED);

            }
        
            RootNavigationController.replace(new OverviewController(s));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
            RootNavigationController.showErrorAlert("Error in creating a new universe");
        }
    }

    public void quitGame() {
        logger.log(Level.INFO, "Exiting");
        try {
            if (h != null && s != null) {
                h.saveGame(s);
            } else {
                if (h != null || s != null) {
                    logger.warning("conflicting status for game handler");
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    boolean existGame() {
        return !storage.listUniverses().isEmpty();
    }

    void tutorialStatusChanged(TutorialStatus status) {
        if (s != null) s.setTutorialStatus(status);
    }

}
