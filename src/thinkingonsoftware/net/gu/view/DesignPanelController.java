/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.Body;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.panels.DesignPanel;
import thinkingonsoftware.net.gu.view.panels.FactoryOverviewPanel;

/**
 *
 * @author LoSboccacc
 */
public class DesignPanelController  implements ViewController{
    
        Player player;
    public DesignPanel designPanel;


    DesignPanelController(Player player, Universe currentUniverse) {
        this.player = player;
    }
    
    
    
    @Override
    public JPanel create() {
        designPanel = new DesignPanel(this, player);
        
        return designPanel;
        
    }

    @Override
    public JPanel restore(Object memento) {
        this.player = (Player) memento;
        return create();
    }

    @Override
    public Object destroy() {
        designPanel = null;
        return player;
    }

    public void componentPanelSelected() {
                RootNavigationController.changeTutorialStatus(TutorialStatus.COMPONENT_PANEL_SELECTED);
    }

    public void shipPanelSelected() {

    }

    

    
}
