/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import thinkingonsoftware.net.gu.model.universe.dao.SpectralClass;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;

/**
 *
 * @author lorenzoboccaccia
 */
public class BodyWidget extends JButton implements MouseListener {

    private static Image defIcon = new ImageIcon("data/templatesystem.png").getImage();
    private static ImageIcon rollover= new ImageIcon("data/hoversystem.png");
    private static ImageIcon pressed = new ImageIcon("data/selectedsystem.png");
    private static ImageIcon fleet = new ImageIcon("data/fleetinsystem.png");
    private static ImageIcon colony = new ImageIcon("data/colonyinsystem.png");  
    private Image icon = defIcon;

    boolean loading = false;
    
    public static BodyWidget createForStarSystem(StarSystem s) {
        
        
        
        BodyWidget b = new BodyWidget();
        b.setText(s.trueName);
        b.setVerticalAlignment(SwingConstants.TOP);
        b.system = s;
        b.setRolloverEnabled(true);
        b.setOpaque(false);
        b.addMouseListener(b);
        
        return b;
        
        
    }
    private boolean underMouse;
    private boolean mousePressed;
    private StarSystem system;
    
    @Override
    public void paint(Graphics grphcs) {
        System.out.println("custom drawing ftw");
        int h = this.getBounds().height;
        int w = this.getBounds().width;
        int x = 0;
        int y = 0;
        
        if (loading == false) {
            synchronized (this) {
                if (!loading) {
                    loading = true;
                    
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            icon = system.getImage();
                            repaint();
                        }
                    }).start();
                }
            }
        }
        
        grphcs.drawImage(icon, x, y, h, w, null);
       
        if (underMouse) {
            grphcs.drawImage(rollover.getImage(), x, y, h, w, this);
        }
        if (mousePressed) {
            grphcs.drawImage(pressed.getImage(), x, y, h, w, this);
            
        }
        
        
        if (!system.getFleetInSystem().isEmpty()) {
            grphcs.drawImage(fleet.getImage(), x, y, h, w, this);
        }
        grphcs.drawImage(colony.getImage(), x, y, h, w, this);
        super.paint(grphcs);
        
    }
    
    @Override
    public void mouseClicked(MouseEvent me) {
        this.mousePressed = true;
    }
    
    @Override
    public void mousePressed(MouseEvent me) {
        this.mousePressed = true;
    }
    
    @Override
    public void mouseReleased(MouseEvent me) {
        this.mousePressed = false;
    }
    
    @Override
    public void mouseEntered(MouseEvent me) {
        this.underMouse = true;
    }
    
    @Override
    public void mouseExited(MouseEvent me) {
        this.underMouse = false;
    }
}
