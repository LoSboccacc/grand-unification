/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;

/**
 *
 * @author LoSboccacc
 */
public interface ViewController {
    public JPanel create();
    public JPanel restore(Object memento);
    public Object destroy();
}
