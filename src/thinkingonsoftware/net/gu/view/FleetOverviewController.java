/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.panels.FleetOverviewPane;

/**
 *
 * @author LoSboccacc
 */
public class FleetOverviewController implements ViewController {
    private final Player player;
    private final Universe currentUniverse;

    public FleetOverviewController(Player p, Universe u) {
        this.player = p;
        this.currentUniverse = u;
    }

    @Override
    public JPanel create() {
        return new FleetOverviewPane(player,currentUniverse,this);
    }

    @Override
    public JPanel restore(Object memento) {
                return new FleetOverviewPane(player,currentUniverse,this);
    }

    @Override
    public Object destroy() {
        return this;
    }
}
