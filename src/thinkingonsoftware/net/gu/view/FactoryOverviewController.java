/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import javax.swing.tree.TreeModel;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.Body;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.panels.FactoryOverviewPanel;

/**
 *
 * @author LoSboccacc
 */
public class FactoryOverviewController implements ViewController{

    Player player;
    public FactoryOverviewPanel factoryOverviewPanel;


    FactoryOverviewController(Player player, Universe currentUniverse) {
        this.player = player;
    }
    
    
    
    @Override
    public JPanel create() {
        factoryOverviewPanel = new FactoryOverviewPanel(this, player);
        
        return factoryOverviewPanel;
        
    }

    @Override
    public JPanel restore(Object memento) {
        this.player = (Player) memento;
        return create();
    }

    @Override
    public Object destroy() {
        factoryOverviewPanel = null;
        return player;
    }

    public void selectedPlanet(Body selected) {
        if (selected.trueName.equals("Earth")) {
            RootNavigationController.changeTutorialStatus(TutorialStatus.EARTH_SELECTED);
        }
        factoryOverviewPanel.displayBodyDetail(selected);
    }
    
    
}
