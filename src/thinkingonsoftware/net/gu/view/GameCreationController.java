/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.view;

import javax.swing.JPanel;
import thinkingonsoftware.net.gu.lib.UniverseHandler;
import thinkingonsoftware.net.gu.model.GameParameters;
import thinkingonsoftware.net.gu.view.panels.GameParameterPanel;

/**
 *
 * @author LoSboccacc
 */
public class GameCreationController implements ViewController {

    GameParameters p = new GameParameters();
    private final UniverseHandler h;
    private final GameController delegate;
    
 

    GameCreationController(UniverseHandler h, GameController delegate) {
        this.h =h;
        this.delegate = delegate;
    }

    @Override
    public JPanel create() {
        return new GameParameterPanel(p,this);
    }

    @Override
    public JPanel restore(Object memento) {
        p=(GameParameters) memento;
        return new GameParameterPanel(p,this);
    }

    @Override
    public Object destroy() {
        return p;
    }

    public void start() {
       
       delegate.startGame(p);
    }
    
}
