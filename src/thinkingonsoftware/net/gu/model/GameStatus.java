/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.IOException;
import thinkingonsoftware.net.gu.model.players.HumanPlayer;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.players.Population;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchTree;
import thinkingonsoftware.net.gu.model.universe.dao.SpecialSystems;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystemBuilder;
import thinkingonsoftware.net.gu.model.universe.dao.Universe;
import thinkingonsoftware.net.gu.view.TutorialStatus;
 
/**
 *
 * @author LoSboccacc
 */
public class GameStatus implements Serializable {
    
    
    private GameParameters gameParameters;
    private Universe currentUniverse;
    private long universalTime;
    private String gameName = "Random Game";

    private Player player;
    Collection<Player> npr;
    private ResearchTree researchTree;
    private SpecialSystems specialSystems;
    private TutorialStatus tutStatus;

    



    public GameStatus(GameParameters p) {
        this.gameParameters = p;
        try {
            this.researchTree = new ResearchTree();
            this.specialSystems = new SpecialSystems();
        } catch (IOException ex) {
            Logger.getLogger(GameStatus.class.getName()).log(Level.SEVERE, null, ex);
        }
        Player hp = HumanPlayer.getHumanPlayer(researchTree,p);
        player = hp;
        
        switch(p.getDifficulty()){
            case EASY:
                hp.addRandomTechLevels(10);
            case NORMAL:
                hp.addRandomTechLevels(10);
            case HARD:         
                
        }
        this.currentUniverse = createNewUniverse(p,hp);
        universalTime = 0;
        gameName = p.getGameName();
    }

    public GameStatus() {

    }

    public String getGameName() {
        return gameName;
    }

    public Universe getCurrentUniverse() {
        return currentUniverse;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public Player getPlayer() {
        return player;
    }

    public GameParameters getGameParameters() {
        return this.gameParameters;
    }
    

    /**
     * @param player the player to set
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    public void initializeUniverse(GameParameters p) {
        
    }

    private  Universe createNewUniverse(GameParameters p, Player player)  {
        
        int size = 0;
        
        switch (p.getSize()) {
            case LARGE:
                size = 1024;
                break;  
            case MEDIUM:
                size = 256;
                break;
            case SMALL:
                size = 64;
                break;
        }
        int count = size;
        Universe generated  = new Universe();

        Population human = Population.generatePlayerRace("Humans");
        StarSystem home = specialSystems.generatePlayerHomeSystem(human,player);
        home.x = size/2;
        home.y = size/2;
        home.z = 0;
        generated.getSystems().add(home);
        count--;
        Set<Point2D> used = new HashSet<Point2D>();
        used.add(new Point(home.x,home.y));
        home.x *=100;
        home.y *=100;
        
        
        while (count > 0) {
            int x = (int) (Math.random() * size);
            int y = (int) (Math.random() * size);
            int z = (int) (Math.random() * 10)-5;
            if (used.contains(new Point(x,y))) continue;
            StarSystem n = StarSystemBuilder.buildRandomSystem();
            n.x=x*100;
            n.y=y*100;
            n.z=z;
            generated.getSystems().add(n);
            count--;
        }
    


      
        
        //singleton.humanPlayer = human;
        //singleton.currentUniverse.controllers.add(human);
        //singleton.currentUniverse.controllers.add(enemy);
        return generated;
    }

    public TutorialStatus getTutorialStatus() {
        return tutStatus;
    }

    public void setTutorialStatus(TutorialStatus tutStatus) {
        this.tutStatus = tutStatus;
    }
    
    
}
