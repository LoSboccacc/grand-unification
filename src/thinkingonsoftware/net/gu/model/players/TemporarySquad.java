/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.util.ArrayList;
import java.util.Collection;

/**
 * this squad doesn't ask new ships and always return all ship as surplus
 * @author LoSboccacc
 */
class TemporarySquad extends Squad {

    
    
    public TemporarySquad() {
    }

    @Override
    public Collection<ShipDesign> getDesiredComposition() {
        Collection<ShipDesign> ret = new ArrayList<ShipDesign>();
        for (Ship s : ships) {
            ret.add(s.design);
        }
        return ret;
    }

    @Override
    public Collection<ShipDesign> getMissingShip() {
        return new ArrayList<ShipDesign>();
    }

    @Override
    public Collection<Ship> getSurplusShip() {
        return ships;
    }
    
    
    
}
