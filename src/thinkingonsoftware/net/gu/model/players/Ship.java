/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class Ship implements  Serializable{
    static final long serialVersionUID = 1L; 
    
    ShipDesign design;
    
    Collection<Component> components; 
    
    //different from design stat as it may have damages applied.
    Map<String,Integer> stats;

    Ship(ShipDesign d) {
        d.lock();
        this.design = d;
        this.components = createComponent(design.components);
        stats = design.getStats();
    }
    
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }    

    private Collection<Component> createComponent(Collection<ComponentDesign> components) {
        Collection<Component> ret = new ArrayList<Component>();
        for (ComponentDesign c: components) {
            ret.add(new Component(c));
        }
        return ret;
    }
}
