/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author LoSboccacc
 */
public abstract class Squad implements Serializable {

    Collection<Ship> ships = new LinkedList<Ship>();

    public abstract Collection<ShipDesign> getDesiredComposition();

    public abstract Collection<ShipDesign> getMissingShip();

    public abstract Collection<Ship> getSurplusShip();

    void addShip(Ship s) {
        ships.add(s);
    }
}
