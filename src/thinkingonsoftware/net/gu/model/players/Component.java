/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author lorenzoboccaccia
 */
public class Component implements  Serializable{
    ComponentDesign design;
    
    //different from design stat as it may have damages applied.
    Map<String,Integer> stats;

    Component(ComponentDesign c) {
        this.design =c;
        this.stats=c.getStats();
    }
}
