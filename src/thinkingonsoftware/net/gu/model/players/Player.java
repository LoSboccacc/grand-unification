/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.IOException;
import thinkingonsoftware.net.gu.model.universe.dao.views.StarSystemMemento;
import thinkingonsoftware.net.gu.model.universe.dao.views.BodyMemento;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.model.subsystems.dao.ComponentCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.PlayerResearchStatus;
import thinkingonsoftware.net.gu.model.subsystems.dao.PlayerResearchStatusBuilder;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchTree;

/**
 *
 * @author LoSboccacc
 */
public class Player implements Serializable {

    private int id;
    protected Collection<StarSystemMemento> knownSystems = new HashSet<StarSystemMemento>();
    protected Collection<BodyMemento> knownBody = new HashSet<BodyMemento>();
    
    protected PlayerResearchStatus research;
    //private Population race;
    protected ComponentCategory defenceResearchBonus;
    protected Set<ComponentDesign> designedComponents = new HashSet<ComponentDesign>();
    protected Set<ShipDesign> designedShips = new HashSet<ShipDesign>();
    protected ComponentCategory engineResearchBonus;
    protected Fleet defaultShipPool = new Fleet();
    protected Set<Fleet> shipsFleets;
    protected ComponentCategory weaponResearchBonus;
    public String trueName = "Alien Empire";

    public Player(ResearchTree t, ComponentCategory wrb, ComponentCategory drb, ComponentCategory erb) {
                            weaponResearchBonus= wrb;
            defenceResearchBonus= drb;
            engineResearchBonus= erb;
        try {
            research = t.initializePlayerResearch(0);
            System.out.println(research);
        } catch (IOException ex) {
            throw new InstantiationError("cannot load research tree");
        }
    }
        public Player(ResearchTree t, int techLevels) {
        try {
            research = t.initializePlayerResearch(techLevels);
            System.out.println(research);
        } catch (IOException ex) {
            throw new InstantiationError("cannot load research tree");
        }
    }

    public Player(Population pop) {
        //this.race = pop;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the knownSystems
     */
    public Collection<StarSystemMemento> getKnownSystems() {
        return knownSystems;
    }

    /**
     * @return the knownBody
     */
    public Collection<BodyMemento> getKnownBody() {
        return knownBody;
    }

    public void addRandomTechLevels(int i) {
        research = PlayerResearchStatusBuilder.addResearchLevels(research, i);
    }
}