/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import thinkingonsoftware.net.gu.model.Event;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author LoSboccacc
 */
public class Fleet implements Serializable {

    Map<Role, Squad> composition = new HashMap<Role, Squad>();

    public Fleet() {
        composition.put(Role.DEFAULT, new TemporarySquad());
    }
    //the conditions by which task generates
    Orders order;

    void addShip(Ship s) {
        this.composition.get(Role.DEFAULT).addShip(s);
    }
}
