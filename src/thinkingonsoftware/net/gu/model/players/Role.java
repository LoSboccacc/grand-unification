/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

/**
 * the role of a squad within a fleet
 * @author LoSboccacc
 */
public enum Role {
    COMBAT,
    ESCORT,
    DECOY, DEFAULT;
}
