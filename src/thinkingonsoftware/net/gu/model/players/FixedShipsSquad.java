/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * this squad doesn't ask new ships and always return all ship as surplus
 *
 * @author LoSboccacc
 */
class FixedShipsSquad extends Squad {

    Collection<ShipDesign> designs = new ArrayList<ShipDesign>();

    public FixedShipsSquad() {
    }

    @Override
    public Collection<ShipDesign> getDesiredComposition() {
        return designs;

    }

    @Override
    public Collection<ShipDesign> getMissingShip() {

        Collection<ShipDesign> ret = new LinkedList<ShipDesign>(designs);

        for (Ship s : ships) {
            ret.remove(s.design);
        }

        return ret;

    }

    @Override
    public Collection<Ship> getSurplusShip() {
        Collection<Ship> ret = new LinkedList(ships);

        for (ShipDesign d : designs) {

            Iterator<Ship> i = ret.iterator();
            while (i.hasNext()) {
                if (i.next().equals(d)) {
                    i.remove();
                    break;
                }
            }
        }

        return ret;
    }
}
