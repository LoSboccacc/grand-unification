/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import thinkingonsoftware.net.gu.model.subsystems.dao.ComponentCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSubSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchTree;

/**
 *
 * @author lorenzoboccaccia
 */
public class ComponentDesign implements Serializable {

    private ComponentCategory category;
    Map<String, Integer> stats;
    public final ResearchSystem system;
    Integer size;
    public String trueName = "Component";

    public ComponentDesign(ComponentCategory c, ResearchSystem s, int size) {
        category = c;
        this.stats = ResearchTree.evaluate(s.properties, s.subsystems, size);
        this.system = s;
        this.size = size;
        trueName = "Size "+size +" "+s.systemKey;
    }

    static public ComponentDesign createComponent(int size, ResearchCategory cat, ResearchSystem s, Collection<ResearchSubSystem> subs) {
        ComponentCategory ct = null;
        Map<String, Integer> st = null;
        if (!cat.contains(s)) {
            throw new IllegalArgumentException("Wrong system for category: " + s + "; " + cat);
        }
        if (!s.contains(subs)) {
            throw new IllegalArgumentException("Wrong subsystem for system: " + subs + "; " + s);
        }


        return new ComponentDesign(ct, s, size);
    }

    Map<String, Integer> getStats() {
        return stats;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ComponentDesign) {
            ComponentDesign cd = (ComponentDesign) obj;
            if (!cd.category.equals(this.category)) {
                return false;
            }
            if (!cd.system.equals(this.system)) {
                return false;
            }
            if (!cd.stats.equals(this.stats)) {
                return false;
            }
            if (!cd.size.equals(this.size)) {
                return false;
            }
            return true;
        }
        return false;
    }
}
