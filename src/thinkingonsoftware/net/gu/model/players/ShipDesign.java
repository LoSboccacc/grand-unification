/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author lorenzoboccaccia
 */
public class ShipDesign implements Serializable {

    Collection<ComponentDesign> components = new ArrayList<ComponentDesign>();
    private Map<String, Integer> stats;
    private boolean locked = false;
    private String designName="";

    void addComponent(ComponentDesign component) {
        if (locked) {
            throw new IllegalStateException("ship design is locked");
        }
        this.components.add(component);
        stats = null;

    }

    public void lock() {
        locked = true;
    }

    Map<String, Integer> getStats() {
        //TODO: if stats null, calculate from components
        return stats;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ShipDesign) {
            ShipDesign other = (ShipDesign) obj;
            if (!other.designName.equals(other.designName)) {
                return false;
            }
            if (!this.components.equals(other.components)) {
                return false;
            }
            return true;
        }
        return false;

    }
}
