/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import thinkingonsoftware.net.gu.model.GameParameters;
import thinkingonsoftware.net.gu.model.subsystems.dao.ComponentCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchTree;

/**
 *
 * @author LoSboccacc
 */
public class HumanPlayer extends Player {
    private static HumanPlayer singleton;
    
    
    private HumanPlayer(ResearchTree t, ComponentCategory wrb,ComponentCategory drb,ComponentCategory erb) {
        super(t,wrb,drb,erb);
trueName = "Human Federation";
        //createStartingFleets(t);
    }

    

    public static synchronized Player getHumanPlayer(ResearchTree t, GameParameters p) {
        if (singleton==null) {
            singleton = new HumanPlayer(t,p.getWeaponResearch(),p.getDefenceResearch(),p.getEngineResearch());

        }
        return singleton;
    }

    private void createStartingFleets(ResearchTree t) {
        
        
        ComponentDesign engine = createEngineDesign();
        ComponentDesign rcs = createRcsDesign();
        ComponentDesign ftl = createFTLEngineDesign();
        
        ComponentDesign cargoBay = createCargoBay();
        
        ComponentDesign armorLight = createLightArmorSpec();
        ComponentDesign armorHeavy = createHeavyArmorSpec();
        
        ComponentDesign weapon = createWeaponDesign();
        
        designedComponents.add(engine);
        designedComponents.add(rcs);
        designedComponents.add(ftl);

                designedComponents.add(cargoBay);

                        designedComponents.add(armorLight);
        designedComponents.add(engine);

                designedComponents.add(engine);

        
        ShipDesign cargo = new ShipDesign();
        cargo.addComponent(engine);
        cargo.addComponent(cargoBay);
        cargo.addComponent(armorLight);
        
        ShipDesign defender = new ShipDesign();
        defender.addComponent(engine);
        defender.addComponent(engine);
        defender.addComponent(engine);
        defender.addComponent(rcs);
        
        defender.addComponent(weapon);
        defender.addComponent(weapon);
        cargo.addComponent(armorHeavy);
        
        designedShips.add(cargo);
        designedShips.add(defender);
        
        Ship c1 = new Ship(cargo);
        Ship c2 = new Ship(cargo);

        
        Ship d1 = new Ship(defender);
        Ship d2 = new Ship(defender);
        Ship d3 = new Ship(defender);

        
        defaultShipPool.addShip(c1);
        defaultShipPool.addShip(c2);
        defaultShipPool.addShip(d1);
        defaultShipPool.addShip(d2);
        defaultShipPool.addShip(d3);

        
    }

    private ComponentDesign createEngineDesign() {
        ResearchCategory c = research.getByCategory(ComponentCategory.ENGINE_CNV);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(2, c, s, s.subsystems);
    }

    private ComponentDesign createRcsDesign() {
          ResearchCategory c = research.getByCategory(ComponentCategory.ENGINE_RCS);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(1, c, s, s.subsystems);
    }

    private ComponentDesign createFTLEngineDesign() {
        ResearchCategory c = research.getByCategory(ComponentCategory.ENGINE_CNV);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(2, c, s, s.subsystems);
    }

    private ComponentDesign createCargoBay() {
        ResearchCategory c = research.getByCategory(ComponentCategory.SPECIAL_CARGO);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(2, c, s, s.subsystems);
    }

    private ComponentDesign createHeavyArmorSpec() {
                ResearchCategory c = research.getByCategory(ComponentCategory.DEFENCE_ARMOR);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(2, c, s, s.subsystems);
    }

    private ComponentDesign createLightArmorSpec() {
                        ResearchCategory c = research.getByCategory(ComponentCategory.DEFENCE_ARMOR);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(1, c, s, s.subsystems);
    }

    private ComponentDesign createWeaponDesign() {
                        ResearchCategory c = research.getByCategory(weaponResearchBonus);
        
        ResearchSystem s = research.getRandomSystem(c.category);

        return ComponentDesign.createComponent(1, c, s, s.subsystems);
    }

}
