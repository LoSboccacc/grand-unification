/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import java.io.Serializable;
import java.util.Collection;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class Population implements Serializable {

    int id;
    public StarSystem homeSector;
    public Collection<StarSystem> coreSectors;
    public Collection<StarSystem> frontierSectors;
    private double preferredGravity;
    private int preferredTemp;

    public static Population generatePlayerRace(String humans) {
        Population ret = new Population();
        ret.trueName = humans;
        ret.preferredGravity = 1.0;
        ret.preferredTemp = 25;
        //atmos???
        return ret;
    }

    public static Population generateEnemyRace() {
        Population ret = new Population();

        ret.preferredGravity = 1.0;
        ret.preferredTemp = 25;
        //atmos???

        return ret;
    }
    public String trueName = "Aliens";

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
