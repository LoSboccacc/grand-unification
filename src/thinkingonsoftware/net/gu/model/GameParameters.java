/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.model.subsystems.dao.ComponentCategory;

/**
 *
 * @author lorenzoboccaccia
 */
public class GameParameters implements Serializable{
    private boolean tutEnabled = true;

    public boolean isTutorialEnabled() {
        return tutEnabled;
    }

    public void setTutorialEnabled(boolean tutEnabled) {
        this.tutEnabled = tutEnabled;
    }
    
    
    public enum Difficulty {
        EASY,
        NORMAL,
        HARD
    }
    public enum Size {
        SMALL,
        MEDIUM,
        LARGE
    }
    private int systems = 1000;
    
    private double size = 1000.0d;
    private Difficulty difficulty = Difficulty.EASY;
    private Size gameSize = Size.SMALL;
    private String gameName ="Random Game";
    private ComponentCategory weaponBonus = ComponentCategory.WEAPON_BEAM;
    private ComponentCategory defenceBonus= ComponentCategory.DEFENCE_ARMOR;
    private ComponentCategory engineBonus = ComponentCategory.ENGINE_FTL;

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public void setSize(Size size) {
        this.gameSize = size;
    }

    public void setWeaponResearch(ComponentCategory componentCategory) {
        this.weaponBonus = componentCategory;
    }

    public void setDefenceResearch(ComponentCategory componentCategory) {
        this.defenceBonus = componentCategory;
    }
    public void setEngineResearch(ComponentCategory componentCategory) {
        this.engineBonus = componentCategory;
    }

    public ComponentCategory getWeaponResearch() {
        return weaponBonus;
    }
    public ComponentCategory getDefenceResearch() {
        return defenceBonus;
    }    public ComponentCategory getEngineResearch() {
        return engineBonus;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    
    

    public int getSystems() {
        return systems;
    }

    public void setSystems(int systems) {
        this.systems = systems;
    }


    public Size getSize() {
        return gameSize;
    }

    public String getGameName() {
        return gameName;
    }
    
    public void setGameName(String name) {
        this.gameName = name;
    }
    
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
