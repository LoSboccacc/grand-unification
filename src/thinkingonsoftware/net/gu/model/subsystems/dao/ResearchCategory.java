/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.io.Serializable;
import java.util.Collection;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class ResearchCategory implements Serializable {
    public Collection<ResearchSystem> systems;
    public final ComponentCategory category;


    
    ResearchCategory(ComponentCategory c) {
        this.category=c;
    }
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public boolean contains(ResearchSystem s) {
        for (ResearchSystem c : systems) {
            if (c.systemKey.equals(s.systemKey)) {
                return true;
            }
        }
        return false;
    }

}
