/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author LoSboccacc
 */
public class PlayerResearchStatusBuilder {

    
    static PlayerResearchStatus buildPlayerResearchStatus(int startingPoints, Map<String,Map<String,Map<String,Map<String,String>>>> techTree) {
        PlayerResearchStatus ret = new PlayerResearchStatus();
        ret.categories = new ArrayList<ResearchCategory>();
        
       for (Map.Entry<String,Map<String,Map<String,Map<String,String>>>> e : techTree.entrySet()) {
           
           ResearchCategory cat = new ResearchCategory(ComponentCategory.resolve(e.getKey()));
           cat.systems = buildSystems(e.getValue());
           ret.categories.add(cat);
           
           
       }
       
       return addResearchLevels(ret, startingPoints);
    }

    private static Collection<ResearchSystem> buildSystems(Map<String, Map<String, Map<String,String>>> value) {
          Collection<ResearchSystem> ret = new ArrayList<ResearchSystem>();
     
        for (Map.Entry<String, Map<String, Map<String, String>>> e : value.entrySet()) {
           
           ResearchSystem cat = new ResearchSystem(e.getKey());
           //TODO: fix this messy cast stuff  placing formula one step below
           Map<String, Map<String, String>> def = e.getValue();
           if (!def.containsKey("techs")||!def.containsKey("stats")) {
               throw new IllegalArgumentException(def==null?"null":def.toString());
           }
           System.out.println(def);
           cat.subsystems = buildSubSystems(def.get("techs"));

           cat.setProperties(def.get("stats"));
           ret.add(cat);
           
           
       }
        
        return ret;
    }

    private static Set<ResearchSubSystem> buildSubSystems(Map<String,String> ss) {
        HashSet<ResearchSubSystem> ret= new HashSet<ResearchSubSystem>();
        for(Map.Entry<String,String> e : ss.entrySet()){
           ret.add(new ResearchSubSystem(e.getKey(),e.getValue()));
        }
       return ret;
    }

    public static PlayerResearchStatus addResearchLevels(PlayerResearchStatus ret, int startingPoints) {
        List<ResearchSubSystem> all = new ArrayList<ResearchSubSystem>(ret.getAllSubSystems());

       
        for (int i=0; i< startingPoints;i++) {
           Collections.shuffle(all);
            final ResearchSubSystem next = all.iterator().next();
            next.researchLevel = next.researchLevel+1;
        }
        
        return ret;
    }
    
}
