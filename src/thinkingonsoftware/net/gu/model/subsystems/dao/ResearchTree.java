/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import com.esotericsoftware.yamlbeans.YamlReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * read at startup and store in memory to protect from mid game changes
 *
 * @author LoSboccacc
 */
public class ResearchTree implements Serializable {

    public static Map<String, Integer> evaluate(Map<String, String> formula, Collection<ResearchSubSystem> subs, int size) {
        Map<String, Integer> ret = new HashMap<String, Integer>();
        for (Map.Entry<String, String> e : formula.entrySet()) {
            //TODO: interpreter for formulas
            ret.put(e.getKey(), 0);
        }
        return ret;
    }
    private String rawTechTree;

    public ResearchTree() throws IOException {

        rawTechTree = readFile("data/technologies.yml");

    }

    public PlayerResearchStatus initializePlayerResearch(int startingPoints) throws IOException {

        Map o = (Map) new YamlReader(rawTechTree).read();
        System.out.append("READEN RESEARCH FROM FILE " + o.toString());
        return PlayerResearchStatusBuilder.buildPlayerResearchStatus(startingPoints, o);

    }

    private static String readFile(String path) throws IOException {
        FileInputStream stream = new FileInputStream(new File(path));
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            return Charset.defaultCharset().decode(bb).toString();
        } finally {
            stream.close();
        }
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
