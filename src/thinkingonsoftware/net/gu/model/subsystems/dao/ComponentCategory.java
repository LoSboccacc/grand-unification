/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

/**
 * Category defines what valid component stats are and how to use them
 * @author LoSboccacc
 */
public enum ComponentCategory {
        ENGINE_FTL,
        ENGINE_CNV,
        ENGINE_RCS,
        WEAPON_BEAM,
        WAEAPON_PROP,
        WEAPON_PROJ,
        DEFENCE_ARMOR,
        DEFENCE_SHIELD,
        DEFENCE_CLOACK, SPECIAL_CARGO;
        
        public static ComponentCategory resolve(String s){
            if (s.equals("ftl_engines")) return ENGINE_FTL;
            if (s.equals("standard_engines")) return ENGINE_CNV;
            if (s.equals("control_systems")) return ENGINE_RCS;
            
            
            if (s.equals("beams")) return WEAPON_BEAM;
            if (s.equals("projectiles")) return WEAPON_PROJ;
            if (s.equals("propelled")) return WAEAPON_PROP;
            
            if (s.equals("armor")) return DEFENCE_ARMOR; 
            if (s.equals("shield")) return DEFENCE_SHIELD; 
            if (s.equals("cloack")) return DEFENCE_CLOACK; 
            
                        
            if (s.equals("cargo")) return SPECIAL_CARGO;                  
           
            throw new IllegalArgumentException("Invalid category: "+s);
        }
    }

