/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class ResearchSystem implements Serializable,Cloneable {

    public Set<ResearchSubSystem> subsystems;
    public final String systemKey;
    public Map<String, String> properties;

    public ResearchSystem(String key) {
        this.systemKey = key;
        this.properties =new HashMap<String, String>();
    }

    public ResearchSystem(ResearchSystem s) {
        this.systemKey = s.systemKey;
        this.subsystems = new HashSet<ResearchSubSystem>();
        for (ResearchSubSystem ss : s.subsystems) {
            subsystems.add(new ResearchSubSystem(ss.key, ss.desc, ss.researchLevel));
        }
        this.properties = new HashMap<String, String>(s.properties);
        
    }

    void setProperties(Map<String, String> def) {
        this.properties = def;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public boolean contains(Collection<ResearchSubSystem> subs) {
        for (ResearchSubSystem sub : subs) {
            if (!contain(sub)) {
                return false;
            }
        }
        return true;
    }

    private boolean contain(ResearchSubSystem sub) {
        for (ResearchSubSystem c : subsystems) {
            if (c.key.equals(sub.key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ResearchSystem) {
            ResearchSystem other = (ResearchSystem) obj;
            if (!other.systemKey.equals(this.systemKey)) {
                return false;
            }
            if (other.subsystems.equals(this.subsystems)) {
                return true;
            }
        }
        return false;
    }
}
