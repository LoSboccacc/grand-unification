/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class PlayerResearchStatus implements Serializable{
    
    Collection<ResearchCategory> categories;
    
    
        @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
        
    public Collection getSubSystemByCategory(ComponentCategory filter) {
                Collection a = new ArrayList();
        
        for (ResearchCategory c :categories) {
            if (c.category != filter) continue;
            for (ResearchSystem s : c.systems) {
                a.addAll(s.subsystems);
            }
        }
        
        return a;
    }
       

    public Collection<ResearchSubSystem> getAllSubSystems() {
        Collection a = new ArrayList();
        
        for (ResearchCategory c :categories) {
            for (ResearchSystem s : c.systems) {
                a.addAll(s.subsystems);
            }
        }
        
        return a;
    }

    public ResearchSystem getRandomSystem(ComponentCategory category) {
        List<ResearchSystem> picks = new LinkedList<ResearchSystem>();
        
        for (ResearchCategory c :categories) {
            if(c.category==category)
            for (ResearchSystem s : c.systems) {
               picks.add(s);
            }
        }
        if (picks.isEmpty()) throw new IllegalArgumentException("No systems for " + category);
        Collections.shuffle(picks);
     
        return picks.get(0);
        
    }

    public ResearchCategory getByCategory(ComponentCategory componentCategory) {
         for (ResearchCategory c :categories) {
             if (c.category == componentCategory) {
                 return c;
             }
         }
         throw new IllegalArgumentException("Category not known for player "+componentCategory);
    }

    
    
}
