/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class ResearchSubSystem implements Serializable {

    public Integer researchLevel;
    public final String key;
    public final String desc;

    ResearchSubSystem(String key, String desc) {
        this(key, desc, 0);
    }

    public ResearchSubSystem(String key, String desc, Integer researchLevel) {
        this.key = key;
        this.desc = desc;
        this.researchLevel = researchLevel;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ResearchSubSystem) {
            ResearchSubSystem other = (ResearchSubSystem) obj;
            if (!other.key.equals(this.key)) {
                return false;
            }
            if (!other.researchLevel.equals(this.researchLevel)) {
                return false;
            }
            return true;
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return key.hashCode() * this.researchLevel;
    }
    
}
