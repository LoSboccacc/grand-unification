/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model;

import java.io.Serializable;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public abstract class Event implements Comparable<Event>, Serializable {


    private long endTime;
    
    @Override
    public int compareTo(Event o) {
        if (o.endTime ==this.endTime) return 0;
        return o.endTime<this.endTime? 1:-1;
    } 
   
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
    
    public abstract void play();
    
}
