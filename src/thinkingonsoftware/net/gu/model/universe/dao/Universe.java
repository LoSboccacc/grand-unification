/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import thinkingonsoftware.net.gu.model.Event;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.PriorityQueue;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.model.players.Player;

/**
 *
 * @author LoSboccacc
 */
public class Universe implements Serializable {
    
    Collection<StarSystem> systems = new HashSet<StarSystem>();
    Collection<Event> orerQueue = new PriorityQueue<Event>();
    Collection<Player> players=new HashSet<Player>();
    
    
    public Universe() {
    }
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

   

    public Collection<StarSystem> getSystems() {
        
        return systems;
    }
  
}
