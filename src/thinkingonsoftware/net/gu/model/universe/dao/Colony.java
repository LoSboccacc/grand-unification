/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import java.io.Serializable;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.players.Population;

/**
 *
 * @author LoSboccacc
 */
class Colony implements Serializable {

    int populationSize;
    Population race;
    Player controller;

    private Colony() {
    }

    public Colony(int populationSize, Population race, Player controller) {
        this.populationSize = populationSize;
        this.race = race;
        this.controller = controller;
    }
    

    public Player getController() {
        return controller;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public Population getRace() {
        return race;
    }

    boolean isControlledBy(Player p) {
        return controller.equals(p);
    }
    int getWorkForce(){
        return populationSize;
    }
}
