/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

/**
 *
 * @author LoSboccacc
 */
public enum SpectralClass {
    O,
    G,
    NEB,
    BH
}
