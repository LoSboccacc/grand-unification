/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

/**
 * I should really not start relying on those too much.
 * @author LoSboccacc
 */
public enum BodyType {
    GENERIC,
    STAR,
    PLANET,
    ASTEROID,
    COMET,
    GAS_GIANT,
    NEBULA,
    BLACK_HOLE
}
