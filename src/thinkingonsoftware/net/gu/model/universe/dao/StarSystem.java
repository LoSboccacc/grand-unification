/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.lib.UniverseHandler;
import thinkingonsoftware.net.gu.model.players.Fleet;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.players.Population;

/**
 *
 * @author LoSboccacc
 */
public class StarSystem implements Serializable {

    
    
    int turbolence;
    int seed;
    
    private transient BufferedImage image;
    
    
    public String disc;
    public String trueName = "Unknown System";
    protected Collection<Body> bodyes = new HashSet<Body>();
    Collection<Fleet> patrolFleets = new HashSet<Fleet>();

    public StarSystem() {
    }
    /**
     * a partial system has only its main body generated
     */
    private boolean partialSystem = true;

    public int x = 200;
    public int y = 200;
    public int z = 5;

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public Point getDrawPosition() {
        return new Point(this.getX(), this.getY());
    }

    public String getTrueName() {
        return trueName;
    }

    public Collection<Body> getBodyes() {
        return bodyes;
    }

    /**
     * @return the disc
     */
    public String getDisc() {
        return disc;
    }

    /**
     * @param disc the disc to set
     */
    public void setDisc(String disc) {
        this.disc = disc;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    public boolean isPartialSystem() {
        return partialSystem;
    }

    public void setPartialSystem(boolean partialSystem) {
        this.partialSystem = partialSystem;
    }

    public Collection<Fleet> getFleetInSystem() {
        ArrayList<Fleet> ret = new ArrayList<Fleet>();

        for (Body b : bodyes) {
            ret.addAll(b.getAllFleets());
        }

        ret.addAll(patrolFleets);

        return ret;
    }
    
    public BufferedImage getImage() {
        if (this.image != null) return this.image;
        
        Body b =  getBrightestStar();
        
        this.image = b.getImage();
        
        return image;
           
    }

    private Body getBrightestStar() {
        Body ret = null;
       for (Body b: bodyes) {
           if (b.type == BodyType.STAR) {
             if (ret ==null) ret =b;
             if (ret.getMagnitude()<b.getMagnitude()) {
                 ret = b;
             }
           }
       }
       if (ret!=null) return ret;
       for (Body b: bodyes) {
           if (b.type == BodyType.NEBULA) {
             if (ret ==null) ret =b;
             if (ret.getMagnitude()<b.getMagnitude()) {
                 ret = b;
             }
           }
       }
       if (ret!=null) return ret;
       for (Body b: bodyes) {
           if (b.type == BodyType.BLACK_HOLE) {
             if (ret ==null) ret =b;
             if (ret.getMagnitude()<b.getMagnitude()) {
                 ret = b;
             }
           }
       }
       if (ret!=null) return ret;
       return ret;
    }
    
}
