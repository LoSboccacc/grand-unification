/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao.views;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.model.universe.dao.Construction;

/**
 *
 * @author lorenzoboccaccia
 */
public class ConstructionMemento extends Construction {

    private Construction originator;

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
