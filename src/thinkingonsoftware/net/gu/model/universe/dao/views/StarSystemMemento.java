/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao.views;

import java.io.Serializable;
import java.util.ArrayList;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.Body;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;

/**
 *
 * @author LoSboccacc
 */
public class StarSystemMemento extends StarSystem implements Serializable {
    
    private StarSystem originator;

    private StarSystemMemento(StarSystem o) {
        originator = o;
        this.trueName = o.trueName;
        this.x = o.x;
        this.z = o.z;
        this.y = o.y;
        
        this.bodyes = new ArrayList<Body>(o.getBodyes());
        
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /**
     * @return the originator
     */
    public StarSystem getOriginator() {
        return originator;
    }

    /**
     * @param originator the originator to set
     */
    public void setOriginator(StarSystem originator) {
        this.originator = originator;
    }
    
    
    public static StarSystemMemento getMementoForPlayer(StarSystem o,Player p) {
        
        for (StarSystemMemento s : p.getKnownSystems()){
            
            //TODO:fix this mess
            if (s.getOriginator()==o) {
                return s;
            }
        }
        
        StarSystemMemento ret = new StarSystemMemento(o);
       
        
        return ret;        
    }

    
}
