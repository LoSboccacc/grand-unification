/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao.views;

import thinkingonsoftware.net.gu.model.universe.dao.Body;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 *
 * @author LoSboccacc
 */
public class BodyMemento extends Body {
    
    private Body originator;
    
    
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }    
}
