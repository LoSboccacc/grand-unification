/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

/**
 *
 * @author lorenzoboccaccia
 */
public enum ConstructionType {
    MINE,
    POWER,
    SOLAR,
    YARD,
    ASSEMBLY,
    STORAGE,
    GENERIC,
    RUIN,
}
