/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import java.util.Collection;
import thinkingonsoftware.net.gu.model.players.Player;

/**
 *
 * @author LoSboccacc
 */
public class Army {

    Collection<GroundUnit> units;
    private Player player;

    public int totalOffence() {
        return 0;
    }

    public int totalDefence() {
        return 0;
    }

    boolean isControlledBy(Player p) {
        return this.player.equals(p);
    }
}
