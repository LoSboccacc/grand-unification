/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import java.io.Serializable;
import thinkingonsoftware.net.gu.model.players.ComponentDesign;
import thinkingonsoftware.net.gu.model.players.Population;

/**
 *
 * @author lorenzoboccaccia
 */
public class Construction implements Serializable {
       
       public ConstructionType type;

    protected Construction() {
    }

        public Construction(ConstructionType t, int scale) {
            type = t;
            this.scale = scale;
        }

       
       
       public Population culture;
       
       public ComponentDesign produced;
           
       int technology;
       int scale;
       
       
}
