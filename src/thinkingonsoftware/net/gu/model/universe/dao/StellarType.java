/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author LoSboccacc
 */
enum StellarType {
  O5("0x9db4ff"),
  B1("0xa2b9ff"),
  B3("0xa7bcff"),
  B5("0xaabfff"),
  B8("0xafc3ff"),
  A1("0xbaccff"),
  A3("0xc0d1ff"),
  A5("0xcad8ff"),
  F0("0xe4e8ff"),
  F2("0xedeeff"),
  F5("0xfbf8ff"),
  F8("0xfff9f9"),
  G2("0xfff5ec"),
  G5("0xfff4e8"),
  G8("0xfff1df"),
  K0("0xffebd1"),
  K4("0xffd7ae"),
  K7("0xffc690"),
  M2("0xffbe7f"),
  M4("0xffbb7b"),
  M6("0xffbb7b");

  
  
  
    public final Color color;

    private StellarType(String colorcode) {
        this.color = Color.decode(colorcode);
    }

    
    static StellarType random() {
        List<StellarType> enu = Arrays.asList(StellarType.values());
        Collections.shuffle(enu);
        return enu.iterator().next();
    }
    
}
