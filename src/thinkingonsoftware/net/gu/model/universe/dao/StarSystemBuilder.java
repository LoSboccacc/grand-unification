/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe.dao;

import java.awt.Color;
import java.util.List;
import java.util.Map;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.players.Population;
import thinkingonsoftware.net.gu.model.universe.dao.views.StarSystemMemento;

/**
 *
 * @author LoSboccacc
 */
public class StarSystemBuilder {

    static StarSystem buildSpecialSystem(List<Map<String, Object>> bodies, Population human, Player player) {

        StarSystem s = new StarSystem();

        for (Map<String, Object> bodyMap : bodies) {
            s.bodyes.add(buildBody((Map<String, Object>) bodyMap.get("body"), human, player, s));
        }

        for (Map<String, Object> bodyMap : bodies) {
            Body b = findBody(s, (String) ((Map<String, Object>) bodyMap.get("body")).get("trueName"));
            Body f = findBody(s, (String) ((Map<String, Object>) bodyMap.get("body")).get("focus"));
            b.setFocus(f);
        }

        for (Body b : s.bodyes) {
            if (b.getFocus() == null) {
                s.trueName = b.getTrueName();
            }
        }
        player.getKnownSystems().add(StarSystemMemento.getMementoForPlayer(s, player));
        return s;
    }

    private static Body buildBody(Map<String, Object> bodyMap, Population pop, Player player, StarSystem o) {


        Body body = new Body(BodyType.valueOf((String) bodyMap.get("type")));

        body.setFocusDistance(parseDouble((String) bodyMap.get("focusDistance")));

        body.bodyPrimary = parseColor((String) bodyMap.get("bodyPrimary"));
        body.bodySecondary = parseColor((String) bodyMap.get("bodySecondary"));
        body.bodyAtmos = parseColor((String) bodyMap.get("bodyAtmos"));
        body.bodyOvercast = parseColor((String) bodyMap.get("bodyOvercast"));

        body.overcastCoverage = parsePercent((String) bodyMap.get("overcastCoverage"));
        body.landCoverage = parsePercent((String) bodyMap.get("landCoverage"));

        body.classification = bodyMap.containsKey("classification") ? StellarType.valueOf((String) bodyMap.get("classification")) : null;


        body.trueName = (String) bodyMap.get("trueName");
        if (bodyMap.get("populated") != null && (bodyMap.get("populated").equals("True") || bodyMap.get("populated").equals("true"))) {
            buildInstallations(body, pop, player);
        }
        return body;
    }

    private static void buildInstallations(Body body, Population pop, Player player) {
        body.buildings.add(new Construction(ConstructionType.SOLAR, 1));
        body.buildings.add(new Construction(ConstructionType.POWER, 10));
        body.buildings.add(new Construction(ConstructionType.MINE, 10));

        body.setChemicalDensity((int)Math.random() * 10 + 5);
        body.setEarthsDensity((int)Math.random() * 10 + 5);
        body.setMetalsDensity((int)Math.random() * 10 + 5);

        body.colonies.add(new Colony(100,pop,player));
        
    }

    private static Body findBody(StarSystem o, String string) {
        if (string == null) {
            return null;
        }
        for (Body b : o.bodyes) {
            if (b.getTrueName().equals(string)) {
                return b;
            }
        }
        return null;
    }

    private static Color parseColor(String string) {
        if (string == null || string.length() == 0) {
            return new Color((float) Math.random(), (float) Math.random(), (float) Math.random());
        }

        return Color.decode(string);

    }

    private static Double parseDouble(String get) {
        if (get == null || get.length() == 0) {
            return null;
        }
        return Double.valueOf(get);
    }

    private static Double parsePercent(String string) {
        Double d = parseDouble(string);
        if (d != null) {
            d = d / 100.0;
        } else {
            d = 0.0;
        }
        return d;

    }

    public static StarSystem buildRandomSystem() {
        StarSystem ret = new StarSystem();
        final Body generateRandomStar = Body.generateRandomStar();
        ret.bodyes.add(generateRandomStar);
        int b = (int) (Math.random() * 20);
        for (int i = 0; i < b; i++) {
            ret.bodyes.add(Body.generateRandomPlanet(generateRandomStar));

        }
        return ret;
    }
}
