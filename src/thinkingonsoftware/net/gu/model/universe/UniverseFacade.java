/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.universe;

import java.util.Collection;
import thinkingonsoftware.net.gu.model.players.Player;
import thinkingonsoftware.net.gu.model.universe.dao.StarSystem;
import thinkingonsoftware.net.gu.model.universe.dao.views.StarSystemMemento;

/**
 *
 * @author lorenzoboccaccia
 */
public class UniverseFacade {
    
    public Collection<StarSystemMemento> getKnowSystems(Player p){
        
        return p.getKnownSystems();
    }

    
    public UniverseFacade() {
    }
    
    
    
    
    
}
