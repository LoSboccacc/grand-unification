/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model;

import java.util.Collection;
import java.util.PriorityQueue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LoSboccacc
 */
public class OrdersTest {
    
    public OrdersTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of compareTo method, of class Orders.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Event oFirst = new Event(){
            public void play() {
            }
        };
        Event oSecond = new Event(){
            public void play() {
            }
        };
        oSecond.setEndTime(100);
        Collection<Event> s = new PriorityQueue<Event>();
        s.add(oSecond);
        s.add(oFirst);
        
        assertEquals(oFirst,s.iterator().next());
        
    }
    
    @Test
    public void testMultipleInsert() {
        System.out.println("multiSet");
        
        System.out.println("compareTo");
        Event oFirst = new Event(){
            public void play() {
            }
        };
        Event oSecond = new Event(){
            public void play() {
            }
        };
        Event oThird = new Event(){
            public void play() {
            }
        };
        Event oFourth = new Event(){
            public void play() {
            }
        };
        oSecond.setEndTime(100);
        oThird.setEndTime(100);
        
        Collection<Event> s = new PriorityQueue<Event>();

        s.add(oSecond);
        s.add(oFourth);
        s.add(oThird);
        s.add(oFirst);
        assertEquals(4, s.size());
        
    }
}
