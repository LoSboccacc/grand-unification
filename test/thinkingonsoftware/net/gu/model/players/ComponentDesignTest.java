/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import thinkingonsoftware.net.gu.model.subsystems.dao.ComponentCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.PlayerResearchStatus;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSubSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchTree;

/**
 *
 * @author LoSboccacc
 */
public class ComponentDesignTest {

    private PlayerResearchStatus tree;

    public ComponentDesignTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        try {
            this.tree = new ResearchTree().initializePlayerResearch(100);
        } catch (IOException iOException) {
            fail(iOException.getMessage());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class ComponentDesign.
     */
    @Test
    public void testEquals1() {

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);




            ComponentDesign obj = new ComponentDesign(c, s, 10);
            ComponentDesign instance = new ComponentDesign(c, s, 10);
            boolean expResult = true;
            boolean result = instance.equals(obj);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of equals method, of class ComponentDesign.
     */
    @Test
    public void testEquals2() {

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);

            ComponentDesign obj = new ComponentDesign(c, s, 10);
            ComponentDesign instance = new ComponentDesign(c, s, 5);
            boolean expResult = false;
            boolean result = instance.equals(obj);
            assertEquals(expResult, result);
        }
    }
    /**
     * Test of equals method, of class ComponentDesign.
     */
    @Test
    public void testEquals3() {

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);
            ResearchSystem s2 = new ResearchSystem(s);
            
            ComponentDesign obj = new ComponentDesign(c, s, 10);
            
            ComponentDesign instance = new ComponentDesign(c, s2, 10);
            boolean expResult = true;
            boolean result = instance.equals(obj);
            assertEquals(expResult, result);
        }
    }
        /**
     * Test of equals method, of class ComponentDesign.
     */
    @Test
    public void testEquals4() {

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);
            ResearchSystem s2 = new ResearchSystem(s);
                      
            
            s2.subsystems.iterator().next().researchLevel = -1;
            ComponentDesign obj = new ComponentDesign(c, s, 10);
            
            ComponentDesign instance = new ComponentDesign(c, s2, 10);
            boolean expResult = false;
            boolean result = instance.equals(obj);
            assertEquals(expResult, result);
        }
    }
}
