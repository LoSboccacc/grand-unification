/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.players;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import thinkingonsoftware.net.gu.model.subsystems.dao.ComponentCategory;
import thinkingonsoftware.net.gu.model.subsystems.dao.PlayerResearchStatus;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSubSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchSystem;
import thinkingonsoftware.net.gu.model.subsystems.dao.ResearchTree;

/**
 *
 * @author LoSboccacc
 */
public class ShipDesignTest {

    private PlayerResearchStatus tree;

    public ShipDesignTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        try {
            this.tree = new ResearchTree().initializePlayerResearch(100);
        } catch (IOException iOException) {
            fail(iOException.getMessage());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class ShipDesign.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals");
        Object obj = new ShipDesign();
        ShipDesign instance = new ShipDesign();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ShipDesign.
     */
    @Test
    public void testEquals2() {
        System.out.println("equals");
        ShipDesign obj = new ShipDesign();
        ShipDesign instance = new ShipDesign();

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);

            ComponentDesign cd = new ComponentDesign(c, s, 10);
            obj.addComponent(cd);
            instance.addComponent(cd);
        }


        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ShipDesign.
     */
    @Test
    public void testEquals3() {
        System.out.println("equals");
        ShipDesign obj = new ShipDesign();
        ShipDesign instance = new ShipDesign();

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);

            ComponentDesign cd = new ComponentDesign(c, s, 10);
            obj.addComponent(cd);
            cd = new ComponentDesign(c, s, 5);
            instance.addComponent(cd);
        }


        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class ShipDesign.
     */
    @Test
    public void testEquals4() {
        System.out.println("equals");
        ShipDesign obj = new ShipDesign();
        ShipDesign instance = new ShipDesign();

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);

            ComponentDesign cd = new ComponentDesign(c, s, 10);
            obj.addComponent(cd);
            ResearchSystem s2 = new ResearchSystem(s);


            Iterator<ResearchSubSystem> i = s2.subsystems.iterator();
            ResearchSubSystem rs = i.next();
            i.remove();
            rs = new ResearchSubSystem(rs.key, rs.desc, rs.researchLevel);
            s2.subsystems.add(rs);

            cd = new ComponentDesign(c, s2, 10);
            instance.addComponent(cd);
        }


        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
       /**
     * Test of equals method, of class ShipDesign.
     */
    @Test
    public void testEquals5() {
        System.out.println("equals");
        ShipDesign obj = new ShipDesign();
        ShipDesign instance = new ShipDesign();

        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);

            ComponentDesign cd = new ComponentDesign(c, s, 10);
            obj.addComponent(cd);
            ResearchSystem s2 = new ResearchSystem(s);


            Iterator<ResearchSubSystem> i = s2.subsystems.iterator();
            ResearchSubSystem rs = i.next();
            i.remove();
            rs = new ResearchSubSystem(rs.key, rs.desc, rs.researchLevel+1);
            s2.subsystems.add(rs);

            cd = new ComponentDesign(c, s2, 10);
            instance.addComponent(cd);
        }


        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
