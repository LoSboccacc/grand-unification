/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.io.IOException;
import java.util.EnumSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LoSboccacc
 */
public class ResearchCategoryTest {
    private PlayerResearchStatus tree;
    
    public ResearchCategoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            this.tree = new ResearchTree().initializePlayerResearch(100);
        } catch (IOException iOException) {
            fail(iOException.getMessage());
        }
    }
    
    @After
    public void tearDown() {
    }

       /**
     * Test of contains method, of class ResearchCategory.
     */
    @Test
    public void testContains() {
        for (ComponentCategory c : EnumSet.allOf(ComponentCategory.class)) {
            ResearchSystem s = tree.getRandomSystem(c);

            ResearchCategory instance = tree.getByCategory(c);
            boolean expResult = true;
            boolean result = instance.contains(s);
            assertEquals(expResult, result);
        }
    }
}
