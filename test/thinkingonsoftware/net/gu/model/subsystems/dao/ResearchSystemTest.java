/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LoSboccacc
 */
public class ResearchSystemTest {

    public ResearchSystemTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class ResearchSystem.
     */
    @Test
    public void testEquals1() {


        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        HashSet<ResearchSubSystem> s2 = new HashSet<ResearchSubSystem>(s1);

        System.out.println("equals1");
        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem("system1");
        instance.subsystems = s2;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class ResearchSystem.
     */
    @Test
    public void testEquals2() {


        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));
        HashSet<ResearchSubSystem> s2 = new HashSet<ResearchSubSystem>();
        s2.add(new ResearchSubSystem("key1", ""));
        s2.add(new ResearchSubSystem("key2", ""));

        System.out.println("equals2");
        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem("system1");
        instance.subsystems = s2;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class ResearchSystem.
     */
    @Test
    public void testEquals3() {


        Set<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));
        Set<ResearchSubSystem> s2 = new HashSet<ResearchSubSystem>();
        s2.add(new ResearchSubSystem("key2", ""));
        s2.add(new ResearchSubSystem("key1", ""));

        System.out.println("equals3");
        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem("system1");
        instance.subsystems = s2;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class ResearchSystem.
     */
    @Test
    public void testEquals4() {


        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));
        HashSet<ResearchSubSystem> s2 = new HashSet<ResearchSubSystem>();
        s2.add(new ResearchSubSystem("key2", ""));
        s2.add(new ResearchSubSystem("key3", ""));

        System.out.println("equals4");
        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem("system1");
        instance.subsystems = s2;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class ResearchSystem.
     */
    @Test
    public void testEquals5() {


        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));
        HashSet<ResearchSubSystem> s2 = new HashSet<ResearchSubSystem>();
        s2.add(new ResearchSubSystem("key2", ""));
        s2.add(new ResearchSubSystem("key1", ""));

        System.out.println("equals5");
        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem("system2");
        instance.subsystems = s2;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    @Test
    public void testEquals6() {


        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);

        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    @Test
    public void testEquals7() {


        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);
        instance.subsystems.iterator().next().researchLevel = -1;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }

    @Test
    public void testEquals8() {
        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);
        instance.properties.clear();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals9() {
        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);
        Iterator<ResearchSubSystem> i = instance.subsystems.iterator();
        ResearchSubSystem rs = i.next();
        i.remove();
        rs = new ResearchSubSystem(rs.key, rs.desc, rs.researchLevel);
        instance.subsystems.add(rs);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals10() {
        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);
        Iterator<ResearchSubSystem> i = instance.subsystems.iterator();
        ResearchSubSystem rs = i.next();
        i.remove();
        rs = new ResearchSubSystem(rs.key+"2", rs.desc, rs.researchLevel);
        instance.subsystems.add(rs);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
        @Test
    public void testEquals11() {
        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);
        Iterator<ResearchSubSystem> i = instance.subsystems.iterator();
        ResearchSubSystem rs = i.next();
        i.remove();
        rs = new ResearchSubSystem(rs.key, rs.desc+"3", rs.researchLevel);
        instance.subsystems.add(rs);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
                @Test
    public void testEquals12() {
        HashSet<ResearchSubSystem> s1 = new HashSet<ResearchSubSystem>();
        s1.add(new ResearchSubSystem("key1", ""));
        s1.add(new ResearchSubSystem("key2", ""));

        ResearchSystem obj = new ResearchSystem("system1");
        obj.subsystems = s1;
        ResearchSystem instance = new ResearchSystem(obj);
        Iterator<ResearchSubSystem> i = instance.subsystems.iterator();
        ResearchSubSystem rs = i.next();
        i.remove();
        rs = new ResearchSubSystem(rs.key, rs.desc, rs.researchLevel+2);
        instance.subsystems.add(rs);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
