/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.model.subsystems.dao;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LoSboccacc
 */
public class ResearchSubSystemTest {
    
    public ResearchSubSystemTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class ResearchSubSystem.
     */
    @Test
    public void testEquals1() {
        System.out.println("equals1");
        Object obj = new ResearchSubSystem("testS", "asd");
        ResearchSubSystem instance = new ResearchSubSystem("testS", "das");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class ResearchSubSystem.
     */
    @Test
    public void testEquals2() {
        System.out.println("equals2");
        Object obj = new ResearchSubSystem("testA", "asd");
        ResearchSubSystem instance = new ResearchSubSystem("testS", "das");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
        /**
     * Test of equals method, of class ResearchSubSystem.
     */
    @Test
    public void testEquals3() {
        System.out.println("equals3");
        ResearchSubSystem obj = new ResearchSubSystem("testS", "asd");
        ResearchSubSystem instance = new ResearchSubSystem("testS", "das");
        instance.researchLevel = 1;
        obj.researchLevel = 10;
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
        /**
     * Test of equals method, of class ResearchSubSystem.
     */
    @Test
    public void testEquals4() {
        System.out.println("equals4");
        ResearchSubSystem obj = new ResearchSubSystem("testS", "asd");
        ResearchSubSystem instance = new ResearchSubSystem("testS", "das");
        instance.researchLevel = 10;
        obj.researchLevel = 10;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
