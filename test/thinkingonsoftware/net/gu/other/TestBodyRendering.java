/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package thinkingonsoftware.net.gu.other;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import thinkingonsoftware.net.gu.lib.FastNoise;
import thinkingonsoftware.net.gu.model.universe.dao.Body;
import thinkingonsoftware.net.gu.model.universe.dao.BodyType;

/**
 *
 * @author LoSboccacc
 */
public class TestBodyRendering {
    
    
    public static void main(String[] args) {
        
        
        testStar();
        
        testPlanet();
        
    }

    private static void testStar() {
        
        final Body b = new Body(BodyType.STAR);
        
        JFrame f = new JFrame() 
        {

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.clearRect(0,0, getWidth(), getHeight());
                g.drawImage(b.getImage(),0, 0, this);
            }
            
        };
        
        f.setBackground(Color.BLACK);
        f.setSize(b.getImage().getWidth(), b.getImage().getHeight());
        f.setVisible(true);
    }
    
    
    
    private static void testPlanet() {
        
        final Body b = new Body(BodyType.PLANET);
        
        b.bodyPrimary = Color.BLUE;
        b.bodySecondary = new Color(75,75,0);
        b.bodyAtmos = Color.CYAN;
        b.bodyOvercast = Color.WHITE;
        b.seed = 21312312;
        b.turbolence = 4;
        
        JFrame f = new JFrame() 
        {

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.clearRect(0,0, getWidth(), getHeight());
                g.drawImage(b.getImage(),0, 0, this);
            }
            
        };
        
        f.setBackground(Color.BLACK);
        f.setSize(b.getImage().getWidth(), b.getImage().getHeight());
        f.setVisible(true);
    }
    
    
    
}
